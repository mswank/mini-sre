;;; Copyright (c) 2008, Matthew Swank                                           
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  
(in-package :mini-sre)

(defmacro sre (&body regexes)
  `(compile-nfa (embed-submatch-tree '(seq ,@regexes))))

(defclass nfa-state () 
  ((out :initarg :out :reader out)))

;;( )------->
(defclass open-submatch (nfa-state) 
  ((submatch :initarg :submatch :reader submatch-of))
  (:documentation "Marks the beginning of a capture."))
(defun open-submatch (out submatch)
  (make-instance 'open-submatch :out out :submatch submatch))

;;( )------->
(defclass close-submatch (nfa-state) 
  ((submatch :initarg :submatch :accessor submatch-of))
  (:documentation "Marks the end of a capture."))
(defun close-submatch (out &optional submatch)
  (make-instance 'close-submatch :out out :submatch submatch))

;;      a
;;( )-------> Character match: char-class = a
(defclass char-state (nfa-state) 
  ((char-class :initarg :char-class :reader char-class))
  (:documentation 
   "Recognizes a character (or event-- char-class can name a function)."))
(defun char-state (char-class out) 
  (make-instance 'char-state :char-class char-class :out out))


;;  -------->
;; /       
;;( )         Split
;; \
;;  -------->
(defclass split-state (nfa-state) 
  ((out :accessor out)
   (out1 :initarg :out1 :accessor out1))
  (:documentation 
   "Advances the machine onto two paths; the source of the N in NFA."))
(defun split-state (out out1) 
  (make-instance 'split-state :out out :out1 out1))

(defun embed-submatch-tree (regex)
  "Marks submatches in an uncompiled regex."
  (cond ((atom regex)
         (values regex nil))
        ((member (first regex) '(sub submatch)) 
         (multiple-value-bind (xformed-regexes submatch-kids) 
             (embed-submatch-tree (rest regex))
           (let ((submatch (submatch submatch-kids)))
             (values (cons submatch xformed-regexes) (list submatch)))))
        (t (multiple-value-bind (xformed-regexes submatch-kids) 
               (embed-submatch-tree (first regex))
             (multiple-value-bind (xformed-regexes1 submatch-kids1) 
                 (embed-submatch-tree (rest regex))
               (values (cons xformed-regexes
                             xformed-regexes1)
                       (append submatch-kids submatch-kids1)))))))

(defgeneric compile-nfa (regex &optional next-state)
  (:documentation "Compiles a regex to an NFA.")
  (:method :around (regex &optional (next-state t))
    (call-next-method regex next-state))
  
  (:method ((regex symbol) &optional next-state)
    (char-state (compile-cce regex) next-state))

  ;; character match
  ;;>( )------->
  ;;       c
  (:method (regex &optional next-state)
    (char-state regex next-state))
  
  (:method ((regex vector) &optional next-state)
    (reduce (lambda (regex state)
              (compile-nfa regex state))
            regex
            :initial-value next-state
            :from-end t))
  
  (:method ((regex list) &optional next-state)
    (if (op-p regex 'regex)
        (compile-nfa-by-type (op-name regex) (op-args regex) next-state)
        (char-state (compile-cce regex) next-state))))

(defun repeat-regex (n regex next-state)
  (do ((i 0 (1+ i))
       (state next-state (compile-nfa regex state)))
      ((>= i n) state)))

(defgeneric compile-nfa-by-type (type args next-state)
  (:documentation "Compiles a regex operation to an NFA.")
  (:method ((type submatch) args next-state)
    (let* ((close-state (close-submatch next-state)) 
           (next-state (compile-nfa `(seq ,@args) close-state))
           (open-state (open-submatch next-state type)))
      (setf (submatch-of close-state) type)
      open-state))

  ;;  ______            ______
  ;;\| e(0) |---\......| e(n) |---\
  ;;/|______|---/......|______|---/
  (:method ((type (eql 'seq)) args next-state)
    (reduce (lambda (regex state)
              (compile-nfa regex state))
            args
            :initial-value next-state
            :from-end t))

  ;;               ____
  ;;     +-------\| e  |-|
  ;;    /--------/|____| |
  ;;   /                 |
  ;;>( )<----------------j
  ;;   \          
  ;;    \-------------------->
  ;;
  (:method ((type (eql '*)) args next-state)
    (let* ((s (split-state nil next-state))
           (arg-state (compile-nfa `(seq ,@args) s)))
      (setf (out s) arg-state)
      s))

  
  ;;  ____
  ;;\| e  |------>( )-------->
  ;;/|____|        |
  ;;   ^           |
  ;;   |___________|
  ;;
  (:method ((type (eql '+)) args next-state)
    (let* ((s (split-state nil next-state))
           (arg-state (compile-nfa `(seq ,@args) s)))
      (setf (out s) arg-state)
      arg-state))

  ;;              ____
  ;;    +-------\| e  |
  ;;   /--------/|____|
  ;;>( )         
  ;;   \--------------->
  ;;
  (:method ((type (eql '?)) args next-state)
    (split-state (compile-nfa `(seq ,@args) next-state) next-state))

  (:method ((type (eql '=)) args next-state)
    (destructuring-bind (n . args) args
      (repeat-regex n `(seq ,@args) next-state)))

  (:method ((type (eql '>=)) args next-state)
    (destructuring-bind (n . args) args
      (compile-nfa `(= ,n ,@args) (compile-nfa `(* ,@args) next-state))))

  (:method ((type (eql '**)) args next-state)
    (destructuring-bind (n m . args) args
      (let* ((diff (- m n))
             (diff-state (repeat-regex diff `(? ,@args) next-state)))
        (compile-nfa `(= ,n ,@args) diff-state))))

  ;;          ______
  ;;    +---\| e(0) |          ______
  ;;   /----/|______|    +---\| e(1) |        ________
  ;;>( )                /----/|______|  +---\| e(n-1) |
  ;;   \------------->( )              /----/|________|
  ;;                    \...........>( )      ______
  ;;                                   \----\| e(n) | 
  ;;                                    +---/|______|
  (:method ((type (eql 'or)) args next-state)
    (cond ((null args) next-state)
          ((null (cdr args))
           (compile-nfa (first args) next-state))
          (t (let ((first-split 
                    (split-state (compile-nfa (first args) next-state)
                                 (compile-nfa (second args) next-state))))
               (reduce (lambda (prev-split regex)
                         (split-state (compile-nfa regex next-state)
                                      prev-split))
                       (cddr args)
                       :initial-value first-split))))))

