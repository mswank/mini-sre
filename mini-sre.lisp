;;; Copyright (c) 2008, Matthew Swank
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  

(defmacro sre (&body regexes)
  `(build-nfa '(seq ,@regexes)))

(defclass nfa-state () 
  ((out :initarg :out :reader out)))

;;      a
;;( )-------> Character match: char-class = a
(defclass char-state (nfa-state) 
  ((char-class :initarg :char-class :reader char-class))
  (:documentation 
   "Recognizes a character (or event-- char-class can name a function)"))
(defun char-state (char-class out) 
  (make-instance 'char-state :char-class char-class :out out))

;;  -------->
;; /       
;;( )         Split
;; \
;;  -------->
(defclass split-state (nfa-state) 
  ((out :accessor out)
   (out1 :initarg :out1 :accessor out1))
  (:documentation 
   "Advances the machine onto two different paths; the source of the N in NFA"))
(defun split-state (out out1) 
  (make-instance 'split-state :out out :out1 out1))

(defvar *seen-states*)

(defmacro with-unique-states ((&rest options) &body body)
  "Provides a context to track nfa-states to avoid duplication"
  (declare (ignore options))
  `(let ((*seen-states* (make-hash-table)))
     ,@body))

(defun seen-p (state)
  (gethash state *seen-states*))
(defun (setf seen-p) (new-val state)
  (setf (gethash state *seen-states*) new-val))

(defun match-p (state-list)
  (find t state-list))

(defgeneric build-nfa (regex &optional next-state)
  (:method :around (regex &optional (next-state t))
    (call-next-method regex next-state))
  
  ;; character match
  ;;>( )------->
  ;;       c
  (:method (regex &optional next-state)
    (char-state regex next-state))
  
  (:method ((regex vector) &optional next-state)
    (reduce (lambda (regex state)
              (build-nfa regex state))
            regex
            :initial-value next-state
            :from-end t))
  
  (:method ((regex list) &optional next-state)
    (if (op-p regex)
        (build-nfa-by-type (op-name regex) regex next-state)
        (if (simple-char-class-p regex)
            (let ((class (car regex)))
              (char-state (lambda (char)
                            (find char class :test #'char=))
                          next-state))
            (error "invalid regex ~s" regex)))))

(defun simple-char-class-p (regex)
  (when (null (cdr regex))
    (typep (car regex) 'sequence)))

(defun op-name (regex) (first regex))
(defun op-p (regex)
  (and (consp regex) 
       (member (first regex) '(seq * + ? = >= ** or))))
  
(defun repeat-regex (n regex next-state)
  (do ((i 0 (1+ i))
       (state next-state (build-nfa regex state)))
      ((>= i n) state)))

(defgeneric build-nfa-by-type (type regex next-state)
  ;;  ______            ______
  ;;\| e(0) |---\......| e(n) |---\
  ;;/|______|---/......|______|---/
  (:method ((type (eql 'seq))  regex next-state)
    (reduce (lambda (regex state)
              (build-nfa regex state))
            (rest regex)
            :initial-value next-state
            :from-end t))
  ;;               ____
  ;;     +-------\| e  |-|
  ;;    /--------/|____| |
  ;;   /                 |
  ;;>( )<----------------j
  ;;   \          
  ;;    \-------------------->
  ;;
  (:method ((type (eql '*))  regex next-state)
    (let* ((s (split-state nil next-state))
           (arg-state (build-nfa `(seq ,@(rest regex)) s)))
      (setf (out s) arg-state)
      s))

  ;;  ____
  ;;\| e  |------>( )-------->
  ;;/|____|        |
  ;;   ^           |
  ;;   |___________|
  ;;
  (:method ((type (eql '+)) regex next-state)
    (let* ((s (split-state nil next-state))
           (arg-state (build-nfa `(seq ,@(rest regex)) s)))
      (setf (out s) arg-state)
      arg-state))
  
  ;;              ____
  ;;    +-------\| e  |
  ;;   /--------/|____|
  ;;>( )         
  ;;   \--------------->
  ;;
  (:method ((type (eql '?)) regex next-state)
    (split-state (build-nfa `(seq ,@(rest regex)) next-state) next-state))

  (:method ((type (eql '=)) regex next-state)
    (destructuring-bind (n . regex) (rest regex)
      (repeat-regex n `(seq ,@regex) next-state)))

  (:method ((type (eql '>=)) regex next-state)
    (destructuring-bind (n . regex) (rest regex)
      (build-nfa `(= ,n ,@regex) (build-nfa `(* ,@regex) next-state))))

  (:method ((type (eql '**)) regex next-state)
    (destructuring-bind (n m . regex) (rest regex)
      (let* ((diff (- m n))
             (diff-state (repeat-regex diff `(? ,@regex) next-state)))
        (build-nfa `(= ,n ,@regex) diff-state))))

  ;;          ______
  ;;    +---\| e(0) |          ______
  ;;   /----/|______|    +---\| e(1) |        ________
  ;;>( )                /----/|______|  +---\| e(n-1) |
  ;;   \------------->( )              /----/|________|
  ;;                    \...........>( )      ______
  ;;                                   \----\| e(n) | 
  ;;                                    +---/|______|
  (:method ((type (eql 'or)) regex next-state)
    (let ((args (rest regex)))
      (cond ((null args) next-state)
            ((null (cdr args))
             (build-nfa (first args) next-state))
            (t (let ((first-split 
                      (split-state (build-nfa (first args) next-state)
                                   (build-nfa (second args) next-state))))
                 (reduce (lambda (prev-split regex)
                           (split-state (build-nfa regex next-state)
                                        prev-split))
                         (cddr args)
                         :initial-value first-split)))))))

(defun initial-state-list (nfa)
  (with-unique-states () (next-states nfa)))

(defun string-match-p (string nfa)
  (match-p (reduce (lambda (state-list char)
                     (step-nfa char state-list))
                   string
                   :initial-value (initial-state-list nfa))))

(defun step-nfa (char state-list)
  (with-unique-states () 
    (loop :for state :in state-list
          :when (char-match state char) 
          :nconc (next-states (out state)))))

(defgeneric next-states (state)
  (:method :around (state)
    (unless (seen-p state)
      (setf (seen-p state) t)
      (call-next-method)))
  
  (:method (state)
    (list state))

  (:method ((state split-state))
    (nconc (next-states (out state))
           (next-states (out1 state)))))


(defgeneric char-match (state char)
  (:method (state char) nil)
  
  (:method ((state char-state) char)
    (char-match (char-class state) char))
  
  (:method ((char-matcher function) char)
    (funcall char-matcher char))
  
  ;;match state, can consume no further chars
  (:method ((char-matcher (eql 't)) char)
    nil)

  (:method ((char-matcher symbol) char)
    ;;we can make a separate dispatch function if we get more of these
    (if (eql char-matcher 'any)
        t
        (funcall char-matcher char)))
  
  (:method ((matching-char character) char)
    (char= matching-char char)))

(defclass buffer-char-stream ()
  ((inner-stream :initarg :inner-stream :reader inner-stream)
   (buffer :initarg :buffer :initform nil :reader buffer)
   (buffer-index :initarg :buffer-index :initform 0 :accessor buffer-index)))

(defun buffer-char-stream (inner-stream &optional buffer (buffer-index 0))
  (make-instance 'buffer-char-stream 
                 :inner-stream inner-stream 
                 :buffer buffer 
                 :buffer-index buffer-index ))

(defgeneric get-char (bc-stream)
  (:method ((stream stream))
    (read-char stream nil nil))
  (:method ((bc-stream buffer-char-stream))
    (with-slots (inner-stream buffer buffer-index) bc-stream
      (cond ((and buffer (< buffer-index (length buffer)))
             (prog1 (elt buffer buffer-index)
               (incf buffer-index)))
            (t (get-char inner-stream))))))

(defgeneric reduce-bc-stream (stream)
  (:method ((stream stream)) stream)
  (:method ((stream buffer-char-stream))
    (with-slots (inner-stream buffer buffer-index) stream
      (cond ((and buffer (>= buffer-index (length buffer)))
             (reduce-bc-stream inner-stream))
            (buffer stream)
            (t (reduce-bc-stream inner-stream))))))

(defvar *longest-match*)

(defclass match-state ()
  ((index :initarg :index :reader index)
   (state-list :initarg :state-list :reader state-list)
   (action :initarg :action :reader action)))

(defun match-state (index state-list action)
  (make-instance 'match-state 
                 :index index 
                 :state-list state-list 
                 :action action))

(defclass lexer ()
  ((match-states :initarg :match-states :reader match-states)))
(defun lexer (match-states)
  (make-instance 'lexer :match-states match-states))

(defun inferior-match-p (match-state)
  (with-slots (index state-list) match-state
    (and (null state-list) 
         (or (null index)
             (< index *longest-match*)))))

(defun no-choices-p (match-state)
  (null (state-list match-state)))

(defun step-match-states (char current-len states)
  (labels ((new-match-state (match-state)
             (with-slots (index state-list action)  match-state
               (let ((new-states (step-nfa char state-list)))
                 (if (match-p new-states)
                     (match-state (setf *longest-match* 
                                        (1+ current-len))
                                  new-states 
                                  action)
                     (match-state index new-states action)))))

           (add-match (new-match-states state)
             (cond ((inferior-match-p state)
                    new-match-states)
                   ((no-choices-p state)
                    (cons state new-match-states))
                   (t (cons (new-match-state state)
                            new-match-states)))))
    (reduce #'add-match states :initial-value nil)))

(defun best-match (match-states)
  (find *longest-match* match-states :key #'index))

(defun find-best-match (in match-states 
                        &optional (buffer (make-array '(0) 
                                                      :element-type 'character 
                                                      :adjustable t 
                                                      :fill-pointer t)))
  (let ((char (get-char in)))
    (if char
        (let ((new-match-states 
               (step-match-states char (length buffer) match-states)))
          (vector-push-extend char buffer)
          (if (every #'no-choices-p new-match-states)
              (values (best-match match-states) *longest-match* buffer)
              (find-best-match in new-match-states buffer)))
        (values (best-match match-states) *longest-match* buffer))))

(defun get-match (in matches)
  (let ((*longest-match* 0))
    (find-best-match in matches)))

(defun maybe-new-stream (match-len buffer stream)
  (if (< match-len (length buffer))
      (buffer-char-stream (reduce-bc-stream stream) buffer match-len)
      stream))

(defun make-token-generator (in lexer)
  (macrolet ((delay (&body body)
               `(setf k (lambda () ,@body))))
    (let ((matches (match-states lexer)) 
          k)
      (labels ((get-token (in)
                 (multiple-value-bind (match len string) (get-match in matches)
                   (if match 
                       (let ((action (action match))
                             (out (subseq string 0 len))
                             (in (maybe-new-stream len string in)))
                         (cond (action
                                (delay (get-token in))
                                (funcall action out))
                               (t (get-token in))))
                       (cond ((plusp (length string))
                              (error "Unmatched Input starting at ~s" string))
                             (t (delay nil) nil))))))
        (delay (get-token in))
        (lambda () (funcall k))))))

(defmacro define-lexer (name &body body)
  (let ((match-states 
         (mapcar (lambda (regex-action)
                   (destructuring-bind (regex &optional action) regex-action
                     `(match-state nil 
                                   (initial-state-list (build-nfa ',regex))
                                   ,action)))
                 body)))
    `(defparameter ,name 
       (lexer (list ,@match-states)))))

;;;tests
;;these usually kill backtracking
(defun as (n)
  (let* ((as (make-list n :initial-element #\a))
         (regex `(seq (? #\a) 
                      ,@(mapcar #'(lambda (a)
                                    `(? ,a))
                                as)
                      ,@as
                      #\a))
         (string (map 'string #'identity (cons #\a as))))
    (time (string-match-p string (build-nfa regex)))))

(defun aorbs (n)
  (let* ((as (make-list n :initial-element #\a))
         (bs (make-list n :initial-element #\b))
         (aorbs (mapcar #'list as bs))
         (regex `(seq (? (or #\a #\b))
                      ,@(mapcar #'(lambda (ab)
                                    `(? (or ,(first ab) ,(second ab))))
                                aorbs)
                      ,@(mapcar #'(lambda (ab)
                                    `(or ,(first ab) ,(second ab)))
                                aorbs)
                      (or #\a #\b)))
         (string (map 'string #'identity (cons #\a as))))
    (time (string-match-p string (build-nfa regex)))))

