;;; Copyright (c) 2008, Matthew Swank                                           
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  
(in-package :mini-sre)

(defclass buffer-char-stream ()
  ((inner-stream :initarg :inner-stream :reader inner-stream)
   (buffer :initarg :buffer :initform nil :reader buffer)
   (buffer-index :initarg :buffer-index :initform 0 :accessor buffer-index))
  (:documentation 
   "A simple stream-like class that allows us to unget unmatched subsequences"))

(defun buffer-char-stream (inner-stream &optional buffer (buffer-index 0))
  (make-instance 'buffer-char-stream 
                 :inner-stream inner-stream 
                 :buffer buffer 
                 :buffer-index buffer-index ))

(defgeneric get-char (bc-stream)
  (:method ((stream stream))
    (read-char stream nil nil))
  (:method ((bc-stream buffer-char-stream))
    (with-slots (inner-stream buffer buffer-index) bc-stream
      (cond ((and buffer (< buffer-index (length buffer)))
             (prog1 (elt buffer buffer-index)
               (incf buffer-index)))
            (t (get-char inner-stream))))))

(defgeneric reduce-bc-stream (stream)
  (:documentation 
   "Removes exhasted bc-streams from the chain of linked inner-streams")
  (:method ((stream stream)) stream)
  (:method ((stream buffer-char-stream))
    (with-slots (inner-stream buffer buffer-index) stream
      (cond ((and buffer (>= buffer-index (length buffer)))
             (reduce-bc-stream inner-stream))
            (buffer stream)
            (t (reduce-bc-stream inner-stream))))))

(defun maybe-new-stream (index buffer stream)
  (if (< index (length buffer))
      (buffer-char-stream (reduce-bc-stream stream) buffer index)
      stream))

