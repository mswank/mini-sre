;;; Copyright (c) 2008, Matthew Swank                                           
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  
(in-package :mini-sre)

(defmacro defconst (name value)
 `(eval-when (:compile-toplevel :load-toplevel :execute)
    (unless (boundp ',name)
      (defconstant ,name ,value))))

;;Charcter sets are drawn from http://srfi.schemers.org/srfi-14/srfi-14.html
(defconst punctuation-char-codes
  #(33  ;!
    34  ;"
    35  ;#
    37  ;%
    38  ;&
    39  ;'
    40  ;(
    41  ;)
    42  ;*
    44  ;,
    45  ;-
    46  ;.
    47  ;/
    58  ;:
    59  ;;
    63  ;?
    64  ;@
    91  ;[
    92  ;\
    93  ;]
    95  ;_
    123 ;{
    125 ;}
    161 ;inverted exclamation mark
    171 ;left-pointing double angle quotation mark
    173 ;soft hyphen
    183 ;middle dot
    187 ;right-pointing double angle quotation mark
    191 ;inverted question mark
    ))

(defconst symbol-char-codes
  #(36  ;$
    43  ;+
    60  ;<
    61  ;=
    62  ;>
    94  ;^
    96  ;`
    124 ;|
    126 ;~
    162 ;cent sign
    163 ;pound sign
    164 ;currency sign
    165 ;yen sign
    166 ;broken bar
    167 ;section sign
    168 ;diaeresis
    169 ;copyright sign
    172 ;not sign
    174 ;registered sign
    175 ;macron
    176 ;degree sign
    177 ;plus-minus sign
    180 ;acute accent
    182 ;pilcrow sign
    184 ;cedilla
    215 ;multiplication sign
    247 ;division sign
    ))

(defconst space-char-codes 
  #(9     ;horizontal tab 
    10    ;lf
    11    ;vertical tab
    12    ;ff
    13    ;cr
    32    ;Space
    160   ;nbsp
    5760  ;ogham space mark
    8192  ;en quad
    8193  ;em quad
    8194  ;en space
    8195  ;em space
    8196  ;three-per-em space
    8197  ;four-per-em space
    8198  ;six-per-em space
    8199  ;figure space
    8200  ;punctuation space
    8201  ;thin space
    8202  ;hair space
    8203  ;zero-width space
    8232  ;line separator
    8233  ;paragraph separator
    8239  ;narrow nbsp
    12288 ;ideographic space
    ))

(defconst blank-char-codes 
  #(9     ;horizontal tab 
    32    ;Space
    160   ;nbsp
    5760  ;ogham space mark
    8192  ;en quad
    8193  ;em quad
    8194  ;en space
    8195  ;em space
    8196  ;three-per-em space
    8197  ;four-per-em space
    8198  ;six-per-em space
    8199  ;figure space
    8200  ;punctuation space
    8201  ;thin space
    8202  ;hair space
    8203  ;zero-width space
    8239  ;narrow nbsp
    12288 ;ideographic space
    ))

(defconst control-char-codes
  #(0   ;null
    31  ;unit separator
    127 ;delete
    159 ;application-program-command
    ))

(defun graphic-p (char)
  (or (alphanumericp char)
      (find (char-code char) punctuation-char-codes)
      (find (char-code char) symbol-char-codes)))

(defun normalize-named-cc (name)
  (case name
    (lower 'lower-case)
    (upper 'upper-case)
    (alpha 'alphabetic)
    ((num digit) 'numeric)
    ((alnum alphanum) 'alphanumeric)
    (punct 'punctuation)
    (graph 'graphic)
    ((space white) 'whitespace)
    (print 'printing)
    (cntrl 'control)
    ((xdigit hex) 'hex-digit)
    (t name)))

(defun normalize-range (exp)
  (with-output-to-string (out)
    (dolist (s exp)
      (cond ((stringp s)
             (write-string s out))
            ((characterp s)
             (write-char s out))
            (t (error "Invalid range token ~s" s))))))

(defun simple-char-class-p (regex)
  (when (null (cdr regex))
    (typep (car regex) 'string)))

(defgeneric compile-cce (regex)
  (:documentation "Compiles a character class expression to a predicate.")
  (:method ((regex list))
    (cond ((simple-char-class-p regex)
           (compile-simple-cc (first regex)))
          ((op-p regex 'char-class) 
           (compile-cce-by-type (op-name regex) (op-args regex)))
          (t (error "invalid regex ~s" regex))))

  (:method ((regex symbol))
    (compile-named-cc (normalize-named-cc regex)))

  (:method ((regex character))
    (lambda (char)
      (char= regex char)))

  (:method ((regex string))
    (let ((len (length regex)))
      (cond ((zerop len)
             (constantly nil))
            ((eql len 1)
             (compile-cce (elt regex 0)))
            (t (error "invalid character class ~s" regex))))))

(defun compile-simple-cc (string)
  (lambda (char)
    (find char string :test #'char=)))

(defgeneric compile-cce-by-type (op args)
  (:documentation "Compiles a character class operation to a predicate.")
  (:method ((op (eql '~)) args)
    "Complement of the union of character classes."
    (let ((ops (mapcar #'compile-cce args)))
      (lambda (char)
        ;;complement of or
        (not (loop :for op :in ops
                   :when (funcall op char) :return t
                   :finally (return nil))))))
  
  (:method ((op (eql 'or)) args)
    "Union of character classes."
    (let ((ops (mapcar #'compile-cce args)))
      (lambda (char)
        (loop :for op :in ops
              :when (funcall op char) :return t
              :finally (return nil)))))

  (:method ((op (eql '&)) args)
    "Intersection of character classes."
    (let ((ops (mapcar #'compile-cce args)))
      (lambda (char)
        (loop :for op :in ops
              :unless (funcall op char) :return nil
              :finally (return t)))))

  (:method ((op (eql '-)) args)
    "Difference between the first character class and the union of the 
subsequent classes."
    (if (null args)
        (constantly t)
        (compile-cce `(& ,(first args) (~ ,@(rest args))))))
  
  (:method ((op (eql '/)) args)
    "Compiles range operation. Arguments denote a set of end point pairs. Each 
pair delimits an inclusive charater range. The predicate is the union of the 
ranges."
    (let* ((range (normalize-range args))
           (len (length range)))
      (if (evenp len)
          (lambda (char)
            (loop :for i :below len :by 2
                  :for lower := (elt range i) :and upper := (elt range (1+ i))  
                  :when (char<= lower char upper) :return t
                  :finally (return nil)))
          (error "Range expression has an odd number of chars ~s" 
                 range)))))


(defgeneric compile-named-cc (name)
  (:method ((name symbol))
    (symbol-function name))

  (:method ((name (eql 'any)))
    (constantly t))

  (:method ((name (eql 'nonl)))
    (lambda (char)
      (not (char= char #\Newline))))

  (:method ((name (eql 'lower-case)))
    #'lower-case-p)

  (:method ((name (eql 'upper-case)))
    #'upper-case-p)

  (:method ((name (eql 'alphabetic)))
    #'alpha-char-p)

  (:method ((name (eql 'numeric)))
    #'digit-char-p)

  (:method ((name (eql 'alphanumeric)))
    #'alphanumericp)

  (:method ((name (eql 'punctuation)))
    (lambda (char)
      (find (char-code char) punctuation-char-codes)))

  (:method ((name (eql 'graphic)))
    ;;lisp has a graphic-char-p, but it doesn't match the posix sense
    #'graphic-p)

  (:method ((name (eql 'blank)))
    (lambda (char)
      (find (char-code char) blank-char-codes)))
  
  (:method ((name (eql 'whitespace)))
    (lambda (char)
      (find (char-code char) space-char-codes)))
  
  (:method ((name (eql 'printing)))
    (lambda (char)
      (or (find (char-code char) space-char-codes)
	  (graphic-p char))))
  
  (:method ((name (eql 'control)))
    (lambda (char)
      (find (char-code char) control-char-codes)))
  
  (:method ((name (eql 'hex-digit)))
    (lambda (char)
      (digit-char-p char 16)))
  
  (:method ((name (eql 'ascii)))
    (lambda (char)
      ;;I think this is marginally more portable.
      ;;I'm not sure #\Null and #\Delete are required to be defined.
      (<= 0 (char-code char) 127))))



