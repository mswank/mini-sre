;;; Copyright (c) 2008, Matthew Swank                                           
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  
(in-package :mini-sre)

(defvar *seen-states*)

(defmacro with-unique-states ((&rest options) &body body)
  "Provides a context to track nfa-states to avoid duplication."
  (declare (ignore options))
  `(let ((*seen-states* (make-hash-table)))
     ,@body))

(defun seen-p (state)
  (gethash state *seen-states*))
(defun (setf seen-p) (new-val state)
  (setf (gethash state *seen-states*) new-val))

(defun match-p (state-list)
  (find t state-list :key #'state))

(defgeneric state (object)
  (:method (o) 
    "Pass-through reader called when we don't use a submatch context"
    o))

(defgeneric string-match-p (string nfa with-submatches-p)
  (:method (string nfa (with-submatches-p null))
    (match-p (reduce 
              (lambda (state-list char)
                (step-nfa char state-list))
              string
              :initial-value (initial-state-list nfa nil)))))

(defgeneric initial-state-list (nfa with-submatches-p)
  (:method (nfa (with-submatches-p null))
    (with-unique-states ()
      (next-states nfa nil))))

(defun step-nfa (char state-list)
  (with-unique-states () 
    (loop :for context :in state-list
          :when (char-match (state context) char) 
          :nconc (next-states (out (state context)) context))))

(defgeneric next-states (state context)
  (:method :around (state context)
    (declare (ignore context))
    (unless (seen-p state)
      (setf (seen-p state) t)
      (call-next-method)))
  
  (:method (state context)
    (declare (ignore context))
    (list state))

  (:method ((state open-submatch) context)
    (next-states (out state) context))

  (:method ((state close-submatch) context)
    (next-states (out state) context))

  (:method ((state split-state) context)
    ;;next-state "owns" the return list
    (nconc (next-states (out state) context)
           (next-states (out1 state) context))))

(defgeneric char-match (state char)
  (:method (state char) 
    (declare (ignore state char))
    nil)
  
  (:method ((state char-state) char)
    (char-match (char-class state) char))
  
  (:method ((char-matcher function) char)
    (funcall char-matcher char))
  
  ;;match state, can consume no further chars
  (:method ((char-matcher (eql 't)) char)
    (declare (ignore char))
    nil)

  (:method ((matching-char character) char)
    (char= matching-char char)))

