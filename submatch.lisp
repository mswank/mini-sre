;;; Copyright (c) 2008, Matthew Swank                                           
;;; All rights reserved.                                                        
;;;                                                                             
;;; Redistribution and use in source and binary forms, with or without          
;;; modification, are permitted provided that the following conditions are met:
;;;                                                                             
;;;     * Redistributions of source code must retain the above copyright        
;;; notice, this list of conditions and the following disclaimer.               
;;;     * Redistributions in binary form must reproduce the above copyright     
;;; notice, this list of conditions and the following disclaimer in the         
;;; documentation and/or other materials provided with the distribution.        
;;;                                                                             
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF      
;;; THE POSSIBILITY OF SUCH DAMAGE.  
(in-package :mini-sre)
(defvar *current-char*)

;;doesn't necessarily belong here, but it's handy
(defmacro with-generator (name (&rest initial-bindings) &body body)
  (let ((k (gensym))
        (end (gensym)))
    `(macrolet ((delay (&body body)
                  `(setf ,',k (lambda () ,@body)))
                (end-gen (&optional eog)
                  `(let ((,',end ,eog))
                     (funcall (setf ,',k (lambda () ,',end))))))
       (let (,k)
         (labels ((,name ,(mapcar #'first initial-bindings)
                    ,@body))
           (delay (,name ,@(mapcar #'second initial-bindings)))
           (lambda () (funcall ,k)))))))

(defclass nfa-context ()
  ((state :initarg :state :reader state)
   (pending-captures :initarg :pending-captures :reader pending-captures)
   (completed-captures :initarg :completed-captures 
                       :reader completed-captures)))

(defun nfa-context (state pending-captures completed-captures)
  (make-instance 'nfa-context :state state 
                 :pending-captures pending-captures  
                 :completed-captures completed-captures))

(defclass capture ()
  ((submatch :initarg :submatch :reader submatch-of)
   (begin :initarg :begin :reader begin)
   (end :initarg :end :reader end)
   (fail :initarg :fail :reader fail)))

(defun capture (submatch begin &optional end fail)
  (make-instance 'capture :submatch submatch :begin begin :end end :fail fail))

(defun find-child-captures (capture complete)
  (let* ((submatch (submatch-of capture))
         (kids (kids submatch)))
    (mapcan (lambda (kid)
              (let ((cap (if complete 
                             (find kid complete :key #'submatch-of)
                             (capture kid nil nil nil))))
                (cons cap (find-child-captures cap complete))))
            kids)))

;; I punted on getting the captures in the correct order while 
;; running the match. Instead I just sort them here.
(defun captures (context string)
  (let ((pending (pending-captures context))
        (state (state context)))
    (when (or (not (eql state t)))
      (warn "context is in an unmatched state"))
    (mapcar (lambda (cap) 
              (when (begin cap)
                (subseq string (begin cap) (end cap))))
            (mapcan (lambda (cap)
                      (cons cap 
                            (find-child-captures cap 
                                                 (completed-captures context))))
                    (reverse pending)))))

(defmethod initial-state-list (nfa  (with-submatches-p (eql 't)))
  (with-unique-states ()
    (next-states nfa (nfa-context nfa nil nil))))

(defmethod string-match-p (string nfa (with-submatches-p (eql 't)))
  (let* ((*current-char* 0)
         (match 
          (match-p (reduce 
                    (lambda (state-list char)
                      (incf *current-char*)
                      (step-nfa char state-list))
                    string
                    :initial-value (initial-state-list nfa 
                                                       with-submatches-p)))))
    (when match (values match string))))


(defmethod next-states (state (context nfa-context))
  (list (nfa-context state 
                     (pending-captures context) 
                     (completed-captures context))))

(defmethod next-states ((state open-submatch) (context nfa-context))
  ;;Push capture onto the pending stack unless the capture is for the same
  ;;submatch as the head of the stack. In that case it means we've started
  ;;over with the submatch, and we need to restore the completed stack
  ;;the point where it was when we started the current pending submatch.
  (let* ((submatch (submatch-of state)))
    (labels ((update-context (pending complete)
               (let ((old-capture (first pending)))
                 (cond 
                   ((null pending)
                    (nfa-context nil 
                                 (list (capture submatch 
                                                *current-char* 
                                                nil 
                                                complete)) 
                                 complete))
                     
                   ;;replace capture
                   ((eql (submatch-of old-capture) submatch)
                    (nfa-context nil 
                                 (cons (capture submatch 
                                                *current-char* 
                                                nil 
                                                (fail old-capture)) 
                                       (rest pending))
                                 (fail old-capture)))
                     
                   ;;push new capture
                   (t (nfa-context nil 
                                   (cons (capture submatch 
                                                  *current-char* 
                                                  nil 
                                                  complete) 
                                         pending) 
                                   complete))))))
      (next-states (out state) 
                   (update-context (pending-captures context)
                                   (completed-captures context))))))

(defmethod next-states ((state close-submatch) (context nfa-context))
  ;; If we have a matching capture, update it. Otherwise
  ;; push captures on to the completed stack until we do.
  (labels ((update-context (pending capture complete)
             (unless capture 
               (error "close-submatch with no matching open-submatch ~a"
                      (submatch-of state)))
             (let ((submatch (submatch-of state)))
               (if (eql (submatch-of capture) submatch)
                   (nfa-context nil 
                                (cons (capture submatch 
                                               (begin capture)
                                               *current-char*
                                               (fail capture))
                                      pending)
                                complete)
                   (update-context (rest pending)
                                   (first pending)
                                   (cons capture complete))))))
      
    (let ((pending (pending-captures context)))
      (next-states (out state)
                   (update-context (rest pending)
                                   (first pending) 
                                   (completed-captures context))))))

(defvar *longest-match*)
(defvar *buffer*)

(defclass match-state ()
  ((index :initarg :index :reader index)
   (context :initarg :context :reader context)
   (state-list :initarg :state-list :reader state-list)
   (action :initarg :action :reader action)))

(defun match-state (index context state-list action)
  (make-instance 'match-state 
                 :index index
                 :context context
                 :state-list state-list 
                 :action action))

(defun inferior-match-p (match-state)
  (with-slots (index state-list) match-state
    (and (null state-list) 
         (or (null index)
             (< index *longest-match*)))))

(defun no-choices-p (match-state)
  (null (state-list match-state)))

(defun step-match-states (char current-len states)
  (labels ((new-match-state (match-state)
             (with-slots (index state-list action)  match-state
               (let* ((new-states (step-nfa char state-list))
                      (old-context (context match-state))
                      (context (match-p new-states)))
                 (cond (context
                        (match-state (setf *longest-match* (1+ current-len))
                                     context
                                     new-states 
                                     action))
                       (t (match-state index old-context new-states action))))))
           
           (add-match (new-match-states state)
             (cond ((inferior-match-p state) new-match-states)
                   ((no-choices-p state) (cons state new-match-states))
                   (t (cons (new-match-state state) new-match-states)))))

    (reduce #'add-match states :initial-value nil)))

(defun best-match (match-states)
  (find *longest-match* match-states :key #'index))

(defun find-best-match (in match-states)
  (let ((char (get-char in)))
    (incf *current-char*)
    (if char
        (let ((new-match-states 
               (step-match-states char (length *buffer*) match-states)))

          (vector-push-extend char *buffer*)
          (if (every #'no-choices-p new-match-states)
              (best-match match-states)
              (find-best-match in new-match-states)))
        (best-match match-states))))

(defun get-match (in matches)
  (let ((*longest-match* 0)
        (*current-char* 0)
        (*buffer* (make-array '(0) 
                              :element-type 'character 
                              :adjustable t 
                              :fill-pointer t)))
    (values (find-best-match in matches) *longest-match* *buffer*)))

(defun make-token-generator (in lexer)
  (with-generator get-token ((in in))
    (multiple-value-bind (match len string) (get-match in lexer)
      (if match 
          (let* ((action (action match))
                 (context (context match))
                 (captures (captures context string))
                 (in (maybe-new-stream len string in)))
            (cond (action
                   (delay (get-token in))
                   (apply action captures))
                  (t (get-token in))))
          (cond ((plusp (length string))
                 (error "Unmatched Input starting at ~s" string))
                (t (end-gen) nil))))))

(defmacro define-lexer (name &body body)
  (let ((match-states 
         (mapcar (lambda (regex-action)
                   (destructuring-bind (regex &optional action) regex-action
                     `(match-state nil 
                                   nil
                                   (let ((*current-char* 0))
                                     (initial-state-list 
                                      (compile-nfa 
                                       (embed-submatch-tree '(sub ,regex))) t))
                                   ,action)))
                 body)))
    `(defparameter ,name 
       (list ,@match-states))))

;;;tests
;;these usually kill backtracking
(defun as (n)
  (let* ((as (make-list n :initial-element #\a))
         (regex `(seq (? #\a) 
                      ,@(mapcar #'(lambda (a)
                                    `(? ,a))
                                as)
                      ,@as
                      #\a))
         (string (map 'string #'identity (cons #\a as))))
    (time (string-match-p string (compile-nfa regex) nil))))

(defun aorbs (n)
  (let* ((as (make-list n :initial-element #\a))
         (bs (make-list n :initial-element #\b))
         (aorbs (mapcar #'list as bs))
         (regex `(seq (? (or #\a #\b))
                      ,@(mapcar #'(lambda (ab)
                                    `(? (or ,(first ab) ,(second ab))))
                                aorbs)
                      ,@(mapcar #'(lambda (ab)
                                    `(or ,(first ab) ,(second ab)))
                                aorbs)
                      (or #\a #\b)))
         (string (map 'string #'identity (cons #\a as))))
    (time (string-match-p string (compile-nfa regex) nil))))